package task2.command;

import functionalInterfaces.Command;

import java.util.Scanner;

//Client
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Document doc = new Document();
        Command openCmd = arg -> doc.open(arg);
        Command closeCmd = doc::close;
        Command saveCmd = new Command() {
            @Override
            public void execute(String arg) {
                doc.save(arg);
            }
        };
        Command addTextCommand = new AddTextCommand(doc);
        CommandManager manager;
        String msg;
        boolean isExit = false;
        System.out.println("Commands with document:\n Open\nClose\nSave\nAdd");
        System.out.println("Enter Exit to stop program");
        while (true) {
            System.out.println("Enter command name:");
            String commandName = in.nextLine();
            switch (commandName) {
                case "Open":
                    System.out.println("Enter message:");
                    msg = in.nextLine();
                    manager = new CommandManager(openCmd);
                    manager.executeCommand(msg);
                    break;
                case "Close":
                    System.out.println("Enter message:");
                    msg = in.nextLine();
                    manager = new CommandManager(closeCmd);
                    manager.executeCommand(msg);
                    break;
                case "Save":
                    System.out.println("Enter message:");
                    msg = in.nextLine();
                    manager = new CommandManager(saveCmd);
                    manager.executeCommand(msg);
                    break;
                case "Add":
                    System.out.println("Enter message:");
                    msg = in.nextLine();
                    ((AddTextCommand) addTextCommand).setArg(msg);
                    manager = new CommandManager(addTextCommand);
                    manager.executeCommand(msg);
                    break;
                case "Exit":
                    isExit = true;
                    break;
                default:
                    System.out.println("Incorrect input! Try one more time");
            }
            if (isExit) {
                break;
            }
        }
    }
}
