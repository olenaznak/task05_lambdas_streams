package task2.command;

import functionalInterfaces.Command;

//Concrete task2.command
public class AddTextCommand implements Command {

    private final Document doc;
    private String arg;

    public AddTextCommand(Document doc, String arg) {
        this.doc = doc;
        this.arg = arg;
    }

    public AddTextCommand(Document doc) {
        this.doc = doc;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    @Override
    public void execute(String arg){
        System.out.println("Document name:" + arg);
        doc.addText(arg);
    }
}
