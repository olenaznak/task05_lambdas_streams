package task2.command;

import functionalInterfaces.Command;

//Invoker
public class CommandManager {
    private Command command;

    public CommandManager(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public void executeCommand(String arg){
        command.execute(arg);
    }
}
