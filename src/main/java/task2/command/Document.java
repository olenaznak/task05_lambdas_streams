package task2.command;

//Receiver
public class Document {
    String str;

    public void open(String s){
        System.out.println("Document was opened " + s);
    }

    public void close(String s) {
        System.out.println("Document was closed " + s);
    }

    public void save(String s) {
        System.out.println("Document was saved " + s);
    }

    public void addText(String s){
        System.out.println("Text was added " + s);
    }
}
