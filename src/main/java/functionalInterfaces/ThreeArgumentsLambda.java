package functionalInterfaces;

@FunctionalInterface
public interface ThreeArgumentsLambda {
    int func (int a, int b, int c);
}
