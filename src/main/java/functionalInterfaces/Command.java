package functionalInterfaces;

@FunctionalInterface
public interface Command {
    void execute(String arg);
}
