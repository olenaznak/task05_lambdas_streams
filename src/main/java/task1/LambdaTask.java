package task1;

import functionalInterfaces.ThreeArgumentsLambda;

import java.util.Scanner;

public class LambdaTask {
    public static void main(String[] args) {

        ThreeArgumentsLambda maxOfThree = (a, b, c) -> a > b ? (a > c ? a : c) : (b > c ? b : c);


        ThreeArgumentsLambda avarageOgThree = (n1, n2, n3) -> (n1 + n2 + n3) / 3;
    }

    public static int getInt() {
        Scanner in = new Scanner(System.in);
        while (!in.hasNextInt()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        return in.nextInt();
    }

    public static String getString() {
        String str;
        Scanner in = new Scanner(System.in);
        while (!in.hasNextLine()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        str = in.nextLine();
        return str;
    }
}
