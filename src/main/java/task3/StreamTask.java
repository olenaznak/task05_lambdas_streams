package task3;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTask {
    private static int LIMIT = 10;
    private static int BOUND = 100;

    public static void main(String[] args) {
        List<Integer> randomNums = Stream.generate(() -> new Random().nextInt(BOUND)).limit(LIMIT).collect(Collectors.toList());

        randomNums.forEach(System.out::println);

        double average = randomNums.stream()
                .mapToInt(Integer::intValue)
                .average()
                .orElseThrow(NoSuchElementException::new);

        System.out.println("Average value - " + average);

        int min = randomNums.stream()
                .mapToInt(Integer::intValue)
                .min()
                .orElseThrow(NoSuchElementException::new);

        System.out.println("Min value - " + min);

        int max = randomNums.stream()
                .mapToInt(Integer::intValue)
                .max()
                .orElseThrow(NoSuchElementException::new);

        System.out.println("Max value " + max);

        int sum = randomNums.stream()
                 .mapToInt(Integer::intValue)
                .sum();

        System.out.println("Sum of numbers (using sum() " + sum);

        int sum2 = randomNums.stream()
                .reduce(Integer::sum)
                .get();

        System.out.println("Sum of numbers (using reduce) " + sum2);

        long biggerThanAverageAmount = randomNums.stream()
                .filter(e -> e > average)
                .count();

        System.out.println("Amount of bigger than average value numbers - "
                + biggerThanAverageAmount);
    }
}
