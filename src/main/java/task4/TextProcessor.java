package task4;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class TextProcessor {

    private List<String> list;
    private Set<String> uniqueWords;

    TextProcessor() {
        list = new ArrayList<>();
        uniqueWords = new HashSet<>();
    }

    public void readText() {
        Scanner in = new Scanner(System.in);
        String str;
        while (true) {
            str = in.nextLine();
            if (str.isEmpty()) {
                break;
            }
            list.add(str);
        }
    }

    // \\W - non word character
    private void findUniqueWords() {
        list.stream()
                .map(str -> str.split("\\W"))
                .forEach(str -> {
                    List<String> words = Arrays.asList(str);
                    uniqueWords.addAll(words);
                });
    }

    public long countUniqueWords() {
        findUniqueWords();
        return uniqueWords.size();
    }

    public List<String> getSortedUniqueWords() {
        findUniqueWords();
        List<String> words = new ArrayList<>(uniqueWords);
        return words.stream().sorted().collect(Collectors.toList());
    }

    public Map<String, Integer> countWordsOccurrence() {
        List<String> wordsList = new ArrayList<>();
        list.stream()
                .map(str -> str.split("\\W"))
                .forEach(str -> {
                    List<String> someWords = Arrays.asList(str);
                    wordsList.addAll(someWords);
                });
        Map<String, Integer> collect =
                wordsList.stream().collect(groupingBy(Function.identity(), summingInt(e -> 1)));
        return collect;
    }

    public Map<String, Integer> countCharsOccurrence() {
        List<String> charsList = new ArrayList<>();
        list.stream()
                .map(str -> str.replaceAll("[\\sA-Z]+", ""))
                .map(str -> str.split(""))
                .forEach(str -> {
                    List<String> someChars = Arrays.asList(str);
                    charsList.addAll(someChars);
                });
        Map<String, Integer> collect =
                charsList.stream().collect(groupingBy(Function.identity(), summingInt(e -> 1)));
        return collect;
    }
}
