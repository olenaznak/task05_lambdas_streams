package task4;

public class Main {
    public static void main(String[] args) {
        TextProcessor tp = new TextProcessor();
        System.out.println("Enter sentences");
        tp.readText();
        System.out.print("Amount of unique words - ");
        System.out.println(tp.countUniqueWords());
        System.out.println("Sorted list of unique words:");
        System.out.println(tp.getSortedUniqueWords());
        System.out.println("Words and amount of their occurrence");
        System.out.println(tp.countWordsOccurrence());
        System.out.println("Chars and amount of their occurrence");
        System.out.println(tp.countCharsOccurrence());
    }
}
